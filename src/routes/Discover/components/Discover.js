import "../styles/_discover.scss";
import DiscoverBlock from "./DiscoverBlock/components/DiscoverBlock";
import React, { Component } from "react";
import axios from "axios";
import config from "../../../config";

export default class Discover extends Component {
  constructor() {
    super();

    this.state = {
      newReleases: [],
      playlists: [],
      categories: [],
    };
  }

  async componentDidMount() {
    const {
      api: { redirectURI, authEndpoint, clientId, baseUrl },
    } = config;
    const hash = window.location.hash;
    let token = window.localStorage.getItem("token");

    if (!token) {
      window.location.href = `${authEndpoint}?client_id=${clientId}&redirect_uri=${redirectURI}&response_type=token`;
    }

    if (hash) {
      token = hash
        .substring(1)
        .split("&")
        .find((elem) => elem.startsWith("access_token"))
        .split("=")[1];

      window.location.hash = "";
      window.localStorage.setItem("token", token);
    }

    if (token) {
      const options = {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };
      const fetchNewRelease = axios.get(
        `${baseUrl}/browse/new-releases`,
        options
      );
      const fetchPlaylists = axios.get(
        `${baseUrl}/browse/featured-playlists`,
        options
      );
      const fetchCategories = axios.get(
        `${baseUrl}/browse/categories`,
        options
      );

      axios
        .all([fetchNewRelease, fetchPlaylists, fetchCategories])
        .then(
          axios.spread((...responses) => {
            const {
              data: { albums },
            } = responses[0];
            const {
              data: { playlists },
            } = responses[1];
            const {
              data: { categories },
            } = responses[2];

            this.setState({ newReleases: albums?.items });
            this.setState({ playlists: playlists?.items });
            this.setState({ categories: categories?.items });
          })
        )
        .catch((errors) => {
          console.log(errors);
        });
    }
  }

  render() {
    const { newReleases, playlists, categories } = this.state;

    return (
      <div className="discover">
        <DiscoverBlock
          text="RELEASED THIS WEEK"
          id="released"
          data={newReleases}
        />
        <DiscoverBlock
          text="FEATURED PLAYLISTS"
          id="featured"
          data={playlists}
        />
        <DiscoverBlock
          text="BROWSE"
          id="browse"
          data={categories}
          imagesKey="icons"
        />
      </div>
    );
  }
}
