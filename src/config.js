const config = {
  api: {
    baseUrl: "https://api.spotify.com/v1",
    authUrl: "https://accounts.spotify.com/api/token",
    clientId: process.env.REACT_APP_SPOTIFY_CLIENT_ID,
    clientSecret: process.env.REACT_APP_SPOTIFY_CLIENT_SECRET,
    redirectURI:  process.env.REACT_APP_REDIRECT_URI,
    authEndpoint: "https://accounts.spotify.com/authorize",
  },
};

export default config;
